﻿using UnityEngine;
using System.Collections;

public class SlowMotion : MonoBehaviour
{
	public bool enableSloMo = true;
    public KeyCode key;
    public float speed = 3.0f;
	
	// Update is called once per frame
	void Update ()
	{
		if (enableSloMo)
		{
			if (Input.GetKey(key))
			{
				Time.timeScale = speed;
			}
			else
			{
				Time.timeScale = 1.0f;
			}

			Time.fixedDeltaTime = 0.02F * Time.timeScale;
		}
	}
}
